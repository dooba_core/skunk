/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_VERSION_H
#define	__SKUNK_VERSION_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Version
#define	SKUNK_VERSION							"1.2.0"

#endif
