/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_CONFIG_H
#define	__SKUNK_CONFIG_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "skunk.h"

// Port Flags
#define	SKUNK_CONFIG_PORT_TRUNK						0x01
#define	SKUNK_CONFIG_PORT_VLAN_UNTAG				0x02
#define	SKUNK_CONFIG_PORT_SNIFF_RX					0x04
#define	SKUNK_CONFIG_PORT_SNIFF_TX					0x08
#define	SKUNK_CONFIG_PORT_SNIFF_OUT					0x10
#define	SKUNK_CONFIG_PORT_SNIFF_MASK				(SKUNK_CONFIG_PORT_SNIFF_RX | SKUNK_CONFIG_PORT_SNIFF_TX | SKUNK_CONFIG_PORT_SNIFF_OUT)

// Default Configuration (All access ports in VLAN ID 1 - Double tagging disabled - No sniffing)
#define	SKUNK_CONFIG_DEFAULT_NAME					"skunk"
#define	SKUNK_CONFIG_DEFAULT_VLAN_FT				0
#define	SKUNK_CONFIG_DEFAULT_VLAN_DT				0
#define	SKUNK_CONFIG_PORT_DEFAULT_FLAGS				(SKUNK_CONFIG_PORT_VLAN_UNTAG)
#define	SKUNK_CONFIG_PORT_DEFAULT_VLAN_ID			1

// Name Max Length
#define	SKUNK_CONFIG_NAME_MAXLEN					32

// SCLI Prompt Buffer Size
#define	SKUNK_CONFIG_PROMPT_BUFSIZE					(SKUNK_CONFIG_NAME_MAXLEN + 5)

// Configuration Signature
#define	SKUNK_CONFIG_SIGNATURE						0x559896aa

// Port Configuration Structure
struct skunk_config_port
{
	// Port Flags
	uint8_t flags;

	// VLAN ID
	uint16_t vlan_id;
};

// Configuration Structure
struct skunk_config
{
	// Device Name
	char name[SKUNK_CONFIG_NAME_MAXLEN];
	uint8_t name_len;

	// VLAN Full Trunking
	uint8_t vlan_ft;

	// VLAN Double Tagging
	uint8_t vlan_dt;

	// Ports
	struct skunk_config_port ports[SKUNK_PORTS];

	// Signature
	uint32_t signature;
};

// Configuration Data
extern struct skunk_config skunk_config;

// Load configuration from EEPROM
extern void skunk_config_load();

// Load default configuration
extern void skunk_config_load_default();

// Apply configuration
extern void skunk_config_apply();

// Save configuration to EEPROM
extern void skunk_config_save();

// Set Device Name
extern void skunk_config_set_name(char *name, uint16_t len);

// Configure Access Port
extern void skunk_config_port_access(uint8_t port_num, uint16_t vlan_id, uint8_t untag);

// Configure Trunk Port
extern void skunk_config_port_trunk(uint8_t port_num, uint8_t untag);

// Configure RX Sniff on Port
extern void skunk_config_port_sniff_rx(uint8_t port_num, uint8_t enable);

// Configure TX Sniff on Port
extern void skunk_config_port_sniff_tx(uint8_t port_num, uint8_t enable);

// Configure Sniff Output on Port
extern void skunk_config_port_sniff_out(uint8_t port_num, uint8_t enable);

// Clear Sniff Configuration on Port
extern void skunk_config_port_sniff_off(uint8_t port_num);

#endif
