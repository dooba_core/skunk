/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <util/delay.h>
#include <avr/eeprom.h>
#include <util/cprintf.h>
#include <ksz9896/regs.h>
#include <ksz9896/vlan.h>

// Internal Includes
#include "substrate.h"
#include "sniff.h"
#include "config.h"

// Configuration Data
struct skunk_config skunk_config;

// SCLI Prompt Buffer
char skunk_config_scli_prompt_buf[SKUNK_CONFIG_PROMPT_BUFSIZE];

// Load configuration from EEPROM
void skunk_config_load()
{
	// Load from EEPROM
	eeprom_read_block(&skunk_config, 0, sizeof(struct skunk_config));

	// Check signature (load default config if none saved in EEPROM)
	if(skunk_config.signature != SKUNK_CONFIG_SIGNATURE)					{ skunk_config_load_default(); }
}

// Load default configuration
void skunk_config_load_default()
{
	uint8_t i;
	struct skunk_config_port *p;

	// General settings
	skunk_config.name_len = csnprintf(skunk_config.name, SKUNK_CONFIG_NAME_MAXLEN, "%s", SKUNK_CONFIG_DEFAULT_NAME);
	skunk_config.vlan_ft = SKUNK_CONFIG_DEFAULT_VLAN_FT;
	skunk_config.vlan_dt = SKUNK_CONFIG_DEFAULT_VLAN_DT;

	// Run through ports
	for(i = 0; i < SKUNK_PORTS; i = i + 1)
	{
		// Configure
		p = &(skunk_config.ports[i]);
		p->flags = SKUNK_CONFIG_PORT_DEFAULT_FLAGS;
		p->vlan_id = SKUNK_CONFIG_PORT_DEFAULT_VLAN_ID;
	}

	// Set Signature
	skunk_config.signature = SKUNK_CONFIG_SIGNATURE;
}

// Apply configuration
void skunk_config_apply()
{
	uint16_t i;
	uint8_t untag;
	uint8_t forward;
	uint8_t v;
	struct skunk_config_port *p;
	struct skunk_config_port *pp;
	uint16_t len;
	uint8_t trunk;
	uint8_t trunk_untag;
	uint8_t j;
	uint8_t ft;

	// Disable Switch
	ksz9896_disable_switch(sw0);

	// Disable VLAN
	ksz9896_vlan_disable(sw0);

	// Apply general settings
	len = csnprintf(skunk_config_scli_prompt_buf, SKUNK_CONFIG_PROMPT_BUFSIZE, "%t ~> ", skunk_config.name, skunk_config.name_len);
	scli_set_prompt_n(skunk_config_scli_prompt_buf, len);
	scli_printf("\r");
	for(i = 0; i < len; i = i + 1)															{ scli_printf(" "); }
	scli_printf("\rApplying Skunk configuration - please wait... ");
	ksz9896_vlan_double_tag(sw0, skunk_config.vlan_dt);
	ksz9896_vlan_replace_null_vid(sw0, 1);
	ksz9896_vlan_egress_filter(sw0, 1, 1);
	ft = skunk_config.vlan_ft;

	// Clear VLAN table
	ksz9896_vlan_clear_vtable(sw0, 0, 1, 0, 0, 0, 0, 0, 0);

	// Run through VLAN table
	for(i = 0; i < KSZ9896_VLAN_COUNT; i = i + 1)
	{
		// Write VLAN table entry
		v = 0;
		untag = 0;
		forward = 0;
		for(j = 0; j < SKUNK_PORTS; j = j + 1)
		{
			p = &(skunk_config.ports[j]);
			if(p->vlan_id == i)																		{ v = 1; }
			untag = untag | ((p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG) ? (1 << j) : 0);
			forward = forward | (((p->vlan_id == i) || ((p->flags & SKUNK_CONFIG_PORT_TRUNK) && ft)) ? (1 << j) : 0);
		}
		if(forward || v)																			{ ksz9896_vlan_write_vtable(sw0, i, 1, 0, 0, 0, i, untag, forward); }
	}

	// Run through ports
	for(i = 0; i < SKUNK_PORTS; i = i + 1)
	{
		// Acquire Port Pointer
		p = &(skunk_config.ports[i]);

		// Configure VLAN trunks if full trunking is disabled
		trunk = (p->flags & SKUNK_CONFIG_PORT_TRUNK) != 0;
		if(trunk && (!ft))
		{
			for(j = 0; j < SKUNK_PORTS; j = j + 1)
			{
				if(i != j)
				{
					pp = &(skunk_config.ports[j]);
					if((pp->flags & SKUNK_CONFIG_PORT_TRUNK) == 0)
					{
						ksz9896_vlan_read_vtable(sw0, pp->vlan_id, 0, 0, 0, 0, 0, &untag, &forward);
						untag = untag | ((p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG) ? (1 << i) : 0);
						forward = forward | (1 << i);
						ksz9896_vlan_write_vtable(sw0, pp->vlan_id, 1, 0, 0, 0, pp->vlan_id, untag, forward);
					}
				}
			}
		}
		trunk_untag = (p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG) != 0;
		ksz9896_vlan_set_port_default(sw0, i, (trunk ? 1 : p->vlan_id), 0, 0);
		ksz9896_vlan_set_port_drop_tagged(sw0, i, (!trunk) || trunk_untag);
		ksz9896_vlan_set_port_drop_untagged(sw0, i, trunk && (!trunk_untag));

		// Sniff
		skunk_sniff_port_rx(i, ((p->flags & SKUNK_CONFIG_PORT_SNIFF_RX) != 0));
		skunk_sniff_port_tx(i, ((p->flags & SKUNK_CONFIG_PORT_SNIFF_TX) != 0));
		skunk_sniff_port_out(i, ((p->flags & SKUNK_CONFIG_PORT_SNIFF_OUT) != 0));
	}

	// Enable Switch
	ksz9896_enable_switch(sw0);

	// Enable VLAN
	ksz9896_vlan_enable(sw0);

	// Inform
	scli_printf("Done!\n");
}

// Save configuration to EEPROM
void skunk_config_save()
{
	// Save to EEPROM
	eeprom_update_block(&skunk_config, 0, sizeof(struct skunk_config));
}

// Set Device Name
void skunk_config_set_name(char *name, uint16_t len)
{
	// Set name
	skunk_config.name_len = csnprintf(skunk_config.name, SKUNK_CONFIG_NAME_MAXLEN, "%t", name, len);
}

// Configure Access Port
void skunk_config_port_access(uint8_t port_num, uint16_t vlan_id, uint8_t untag)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Configure
	p->vlan_id = vlan_id;
	p->flags = p->flags & ~SKUNK_CONFIG_PORT_TRUNK;
	if(untag)																{ p->flags = p->flags | SKUNK_CONFIG_PORT_VLAN_UNTAG; }
	else																	{ p->flags = p->flags & ~SKUNK_CONFIG_PORT_VLAN_UNTAG; }
}

// Configure Trunk Port
void skunk_config_port_trunk(uint8_t port_num, uint8_t untag)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Configure
	p->flags = p->flags | SKUNK_CONFIG_PORT_TRUNK;
	if(untag)																{ p->flags = p->flags | SKUNK_CONFIG_PORT_VLAN_UNTAG; }
	else																	{ p->flags = p->flags & ~SKUNK_CONFIG_PORT_VLAN_UNTAG; }
}

// Configure RX Sniff on Port
void skunk_config_port_sniff_rx(uint8_t port_num, uint8_t enable)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Disable Sniff Output if RX enabled
	if(enable)																{ skunk_config_port_sniff_out(port_num, 0); }

	// Configure
	if(enable)																{ p->flags = p->flags | SKUNK_CONFIG_PORT_SNIFF_RX; }
	else																	{ p->flags = p->flags & ~SKUNK_CONFIG_PORT_SNIFF_RX; }
}

// Configure TX Sniff on Port
void skunk_config_port_sniff_tx(uint8_t port_num, uint8_t enable)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Disable Sniff Output if TX enabled
	if(enable)																{ skunk_config_port_sniff_out(port_num, 0); }

	// Configure
	if(enable)																{ p->flags = p->flags | SKUNK_CONFIG_PORT_SNIFF_TX; }
	else																	{ p->flags = p->flags & ~SKUNK_CONFIG_PORT_SNIFF_TX; }
}

// Configure Sniff Output on Port
void skunk_config_port_sniff_out(uint8_t port_num, uint8_t enable)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Disable RX/TX Sniffing if Output enabled
	if(enable)																{ skunk_config_port_sniff_off(port_num); }

	// Configure
	if(enable)																{ p->flags = p->flags | SKUNK_CONFIG_PORT_SNIFF_OUT; }
	else																	{ p->flags = p->flags & ~SKUNK_CONFIG_PORT_SNIFF_OUT; }
}

// Clear Sniff Configuration on Port
void skunk_config_port_sniff_off(uint8_t port_num)
{
	struct skunk_config_port *p;

	// Acquire port
	p = &(skunk_config.ports[port_num]);

	// Configure
	p->flags = p->flags & ~SKUNK_CONFIG_PORT_SNIFF_MASK;
}
