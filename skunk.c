/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <util/delay.h>
#include <ksz9896/regs.h>

// Internal Includes
#include "sniff.h"
#include "config.h"
#include "substrate.h"
#include "skunk.h"

// Main Initialization (eloop)
void init()
{
	// Setup LEDs
	dio_output(SKUNK_LED_STATUS);
	dio_output(SKUNK_LED_ACT);
	dio_lo(SKUNK_LED_STATUS);
	dio_lo(SKUNK_LED_ACT);

	// Initialize Substrate
	substrate_init();

	// Get KSZ device ID
	scli_printf(" - Chip ID: %H\n", ksz9896_get_chip_id(sw0));
	scli_printf(" - CStraps: %h\n", ksz9896_read_reg(sw0, KSZ9896_REG_LED_CONF_STRAP));

	// Initialize Switch
	ksz9896_write_reg(sw0, KSZ9896_REG_OCLK_CTL, 0);			// Disable CLKO_25_125

	// Load Configuration
	skunk_config_load();

	// Apply Configuration
	skunk_config_apply();

	// Flash LEDs
	skunk_init_leds();
	skunk_init_leds();

	// Ready
	dio_hi(SKUNK_LED_STATUS);
	scli_prompt();
}

// Main Loop (eloop)
void loop()
{
	// Update Substrate
	substrate_loop();
}

// Init LEDs
void skunk_init_leds()
{
	uint8_t x;
	uint8_t i;

	// Override
	ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OVERRIDE, 0x000000ff);

	// LEDs off
	ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OUTPUT, 0x000000ff);

	// Seq0 - On
	x = 0x000000ff;
	for(i = 0; i < SKUNK_PORTS; i = i + 1)
	{
		x = x & ~((uint32_t)1 << ((i * 2) + 1));
		ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OUTPUT, x);
		_delay_ms(SKUNK_INIT_LED_DELAY);
		x = x & ~((uint32_t)1 << (i * 2));
		ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OUTPUT, x);
		_delay_ms(SKUNK_INIT_LED_DELAY);
	}

	// Seq1 - Off
	for(i = 0; i < SKUNK_PORTS; i = i + 1)
	{
		x = x | ((uint32_t)1 << ((i * 2) + 1));
		ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OUTPUT, x);
		_delay_ms(SKUNK_INIT_LED_DELAY);
		x = x | ((uint32_t)1 << (i * 2));
		ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OUTPUT, x);
		_delay_ms(SKUNK_INIT_LED_DELAY);
	}

	// Drop Override
	ksz9896_write_reg_l(sw0, KSZ9896_REG_LED_OVERRIDE, 0x00000000);
}
