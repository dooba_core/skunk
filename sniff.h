/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SNIFF_H
#define	__SKUNK_SNIFF_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "skunk.h"

// Set RX sniff on port
extern void skunk_sniff_port_rx(uint8_t port_num, uint8_t enable);

// Set TX sniff on port
extern void skunk_sniff_port_tx(uint8_t port_num, uint8_t enable);

// Set sniff output on port
extern void skunk_sniff_port_out(uint8_t port_num, uint8_t enable);

#endif
