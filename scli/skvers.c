/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <scom/term.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "substrate.h"
#include "version.h"
#include "skvers.h"

// Firmware Version
void skunk_scli_vers(char **args, uint16_t *args_len)
{
	// Display version
	scli_printf("Skunk Firmware %s\n", SKUNK_VERSION);
}
