/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <scom/term.h>
#include <util/str.h>
#include <util/cprintf.h>
#include <ksz9896/regs.h>
#include <ksz9896/vlan.h>
#include <ksz9896/ksz9896.h>

// Internal Includes
#include "substrate.h"
#include "config.h"
#include "skvlan.h"

// VLAN Management
void skunk_scli_vlan(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Operation
	if(str_next_arg(args, args_len, &x, &l) == 0)											{ l = 0; }
	if(l == 0)																				{ scli_printf("Invalid operation - use 'skvlan help'\n"); }
	else if(str_cmp(x, l, SKUNK_SCLI_VLAN_OP_HELP, strlen(SKUNK_SCLI_VLAN_OP_HELP)) == 0)	{ skunk_scli_vlan_help(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_VLAN_OP_FTRK, strlen(SKUNK_SCLI_VLAN_OP_FTRK)) == 0)	{ skunk_scli_vlan_ftrk(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_VLAN_OP_DTAG, strlen(SKUNK_SCLI_VLAN_OP_DTAG)) == 0)	{ skunk_scli_vlan_dtag(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_VLAN_OP_VTAB, strlen(SKUNK_SCLI_VLAN_OP_VTAB)) == 0)	{ skunk_scli_vlan_vtab(args, args_len); }
	else																					{ scli_printf("Invalid operation '%t' - use 'skvlan help'\n", x, l); }
}

// Show help
void skunk_scli_vlan_help(char **args, uint16_t *args_len)
{
	// Show help
	scli_printf("Usage: skvlan <OPERATION> [ARGS]\n");
	scli_printf("\nOperations:\n");
	scli_printf(" * help       - Display this help\n");
	scli_printf(" * ftrk [1/0] - Configure full trunking (enables if no value is specified)\n");
	scli_printf(" * dtag [1/0] - Configure double tagging (enables if no value is specified)\n");
	scli_printf(" * vtab       - Display VLAN table\n");
}

// Configure double tagging
void skunk_scli_vlan_dtag(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t en;

	// Enable by default
	en = 1;

	// Peek enable
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get enable
		if(str_next_arg(args, args_len, &x, &l) == 0)										{ l = 0; }
		if(l > 0)																			{ en = atoi(x); }
	}

	// Configure & apply
	scli_printf("%sabling VLAN double tagging... ", (en ? "En" : "Dis"));
	skunk_config.vlan_dt = (en != 0);
	skunk_config_apply();
}

// Configure full trunking
void skunk_scli_vlan_ftrk(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	uint8_t en;

	// Enable by default
	en = 1;

	// Peek enable
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get enable
		if(str_next_arg(args, args_len, &x, &l) == 0)										{ l = 0; }
		if(l > 0)																			{ en = atoi(x); }
	}

	// Configure & apply
	scli_printf("%sabling VLAN full trunking... ", (en ? "En" : "Dis"));
	skunk_config.vlan_ft = (en != 0);
	skunk_config_apply();
}

// VLAN Table Management
void skunk_scli_vlan_vtab(char **args, uint16_t *args_len)
{
	uint16_t i;
	uint8_t valid;
	uint8_t forward;
	uint8_t priority;
	uint8_t mstp_index;
	uint8_t fid;
	uint8_t port_untag;
	uint8_t port_forward;
	uint8_t nvalid;
	uint8_t nforward;
	uint8_t npriority;
	uint8_t nmstp_index;
	uint8_t nfid;
	uint8_t nport_untag;
	uint8_t nport_forward;
	uint16_t seq;
	uint8_t j;

	// Run through VLAN table
	i = 0;
	scli_printf("VLAN Table:\n");
	while(i < KSZ9896_VLAN_COUNT)
	{
		// Read table entry
		ksz9896_vlan_read_vtable(sw0, i, &valid, &forward, &priority, &mstp_index, &fid, &port_untag, &port_forward);

		// Find sequence
		seq = 0;
		nvalid = valid;
		nforward = forward;
		npriority = priority;
		nmstp_index = mstp_index;
		nfid = fid;
		nport_untag = port_untag;
		nport_forward = port_forward;
		while(((i + seq) < KSZ9896_VLAN_COUNT) && (nvalid == valid) && (nforward == forward) && (npriority == priority) && (nmstp_index == mstp_index) && (nport_untag == port_untag) && (nport_forward == port_forward))
		{
			seq = seq + 1;
			ksz9896_vlan_read_vtable(sw0, i + seq, &nvalid, &nforward, &npriority, &nmstp_index, &nfid, &nport_untag, &nport_forward);
		}

		// Display if valid
		if(valid)
		{
			scli_printf(" * VLAN ");
			if(seq > 1)																			{ scli_printf("%i-%i", i, i + (seq - 1)); }
			else																				{ scli_printf("%i", i); }
			scli_printf(":");
			for(j = 0; j < SKUNK_PORTS; j = j + 1)												{ if(port_forward & (1 << j)) { scli_printf(" / port%i%s", j, ((port_untag & (1 << j)) ? ":untag" : "")); } }
			scli_printf("\n");
		}

		// Next
		i = i + seq;
	}
}
