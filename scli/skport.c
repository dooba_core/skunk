/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <scom/term.h>
#include <util/str.h>
#include <util/hdump.h>
#include <util/cprintf.h>
#include <ksz9896/regs.h>
#include <ksz9896/ksz9896.h>

// Internal Includes
#include "substrate.h"
#include "sniff.h"
#include "config.h"
#include "skunk.h"
#include "skport.h"

// Port Management
void skunk_scli_port(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Operation
	if(str_next_arg(args, args_len, &x, &l) == 0)																	{ l = 0; }
	if(l == 0)																										{ scli_printf("Invalid operation - use 'skport help'\n"); }
	else if(isnum(x[0]))																							{ skunk_scli_port_p(args, args_len, atoi(x)); }
	else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_HELP, strlen(SKUNK_SCLI_PORT_OP_HELP)) == 0)							{ skunk_scli_port_help(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SHOW, strlen(SKUNK_SCLI_PORT_OP_SHOW)) == 0)							{ skunk_scli_port_show_all(args, args_len); }
	else																											{ scli_printf("Invalid operation '%t' - use 'skport help'\n", x, l); }
}

// Individual Port Management
void skunk_scli_port_p(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;

	// Check port
	if(port_num >= SKUNK_PORTS)																						{ scli_printf("Invalid port number '%i'\n", port_num); return; }

	// Loop
	while(*args_len > 0)
	{
		// Acquire Port Operation
		if(str_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
		if(l == 0)																									{ scli_printf("Invalid port operation - use 'skport help'\n"); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SHOW, strlen(SKUNK_SCLI_PORT_OP_SHOW)) == 0)						{ skunk_scli_port_show(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_ACCESS, strlen(SKUNK_SCLI_PORT_OP_ACCESS)) == 0)					{ skunk_scli_port_access(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_TRUNK, strlen(SKUNK_SCLI_PORT_OP_TRUNK)) == 0)						{ skunk_scli_port_trunk(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SNIFF_RX, strlen(SKUNK_SCLI_PORT_OP_SNIFF_RX)) == 0)				{ skunk_scli_port_sniff_rx(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SNIFF_TX, strlen(SKUNK_SCLI_PORT_OP_SNIFF_TX)) == 0)				{ skunk_scli_port_sniff_tx(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SNIFF_OUT, strlen(SKUNK_SCLI_PORT_OP_SNIFF_OUT)) == 0)				{ skunk_scli_port_sniff_out(args, args_len, port_num); }
		else if(str_cmp(x, l, SKUNK_SCLI_PORT_OP_SNIFF_OFF, strlen(SKUNK_SCLI_PORT_OP_SNIFF_OFF)) == 0)				{ skunk_scli_port_sniff_off(args, args_len, port_num); }
		else																										{ scli_printf("Invalid port operation '%t' - use 'skport help'\n", x, l); }
	}
}

// Show Help
void skunk_scli_port_help(char **args, uint16_t *args_len)
{
	// Show help
	scli_printf("Usage: skport [PORT_NUM] <OPERATION> [ARGS] [OPERATION [ARGS] ...]\n");
	scli_printf("\nGeneral Operations:\n");
	scli_printf(" * help                   - Display this help\n");
	scli_printf(" * show                   - Show info for all ports\n");
	scli_printf("\nPort-specific Operations:\n");
	scli_printf(" * show                   - Show port info\n");
	scli_printf(" * access [VLAN_ID] [tag] - Configure as access port for VLAN_ID (default: 1)\n");
	scli_printf("                            Add 'tag' to include the VLAN tags in egress frames\n");
	scli_printf(" * trunk [untag]          - Configure as trunk port\n");
	scli_printf("                            Add 'untag' to strip the VLAN tags in egress frames\n");
	scli_printf(" * sniff_rx [1/0]         - Configure RX sniff on port (enables if no value is specified)\n");
	scli_printf(" * sniff_tx [1/0]         - Configure TX sniff on port (enables if no value is specified)\n");
	scli_printf(" * sniff_out [1/0]        - Configure sniff output on port (enables if no value is specified)\n");
	scli_printf(" * sniff_off              - Clear all sniff options on port\n");
}

// Show All
void skunk_scli_port_show_all(char **args, uint16_t *args_len)
{
	uint8_t i;

	// Show all
	for(i = 0; i < SKUNK_PORTS; i = i + 1)																			{ skunk_scli_port_show(args, args_len, i); scli_printf("\n"); }
}

// Show Port
void skunk_scli_port_show(char **args, uint16_t *args_len, uint8_t port_num)
{
	uint16_t r16;
	uint16_t spd;
	uint8_t fdpx;
	uint8_t link;
	struct skunk_config_port *p;

	// Get port info
	p = &(skunk_config.ports[port_num]);
	r16 = ksz9896_read_reg_s(sw0, KSZ9896_REG_PHY_CTL(port_num));
	spd = 0;
	if(r16 & KSZ9896_REG_PHY_CTL_SPD10)																				{ spd = 10; }
	if(r16 & KSZ9896_REG_PHY_CTL_SPD100)																			{ spd = 100; }
	if(r16 & KSZ9896_REG_PHY_CTL_SPD1000)																			{ spd = 1000; }
	fdpx = (r16 & KSZ9896_REG_PHY_CTL_FULLDUPLEX) != 0;
	r16 = ksz9896_read_reg_s(sw0, KSZ9896_REG_PHY_BASIC_STATUS(port_num));
	link = (r16 & KSZ9896_REG_PHY_BASIC_STATUS_LINK_STATUS) != 0;

	// Show port status
	scli_printf("Port %i: link %s", port_num, (link ? "UP" : "DOWN"));
	if(link)																										{ scli_printf(" %i Mb/s %s-duplex", spd, (fdpx ? "full" : "half")); }
	scli_printf("\n");

	// Show port configuration
	scli_printf(" * Mode: %s", ((p->flags & SKUNK_CONFIG_PORT_TRUNK) ? "trunk" : "access"));
	if(p->flags & SKUNK_CONFIG_PORT_TRUNK)
	{
		if(p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG)																	{ scli_printf(" untag"); }
	}
	else
	{
		if((p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG) == 0)															{ scli_printf(" tag"); }
		scli_printf(" - VLAN %i", p->vlan_id);
	}
	scli_printf("\n");

	// Show port sniffing configuration
	if(p->flags & SKUNK_CONFIG_PORT_SNIFF_MASK)
	{
		scli_printf(" * Sniff:");
		if(p->flags & SKUNK_CONFIG_PORT_SNIFF_RX)																	{ scli_printf(" RX"); }
		if(p->flags & SKUNK_CONFIG_PORT_SNIFF_TX)																	{ scli_printf(" TX"); }
		if(p->flags & SKUNK_CONFIG_PORT_SNIFF_OUT)																	{ scli_printf(" Output"); }
		scli_printf("\n");
	}
}

// Setup Access Port
void skunk_scli_port_access(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;
	uint16_t vid;
	uint8_t tag;

	// Set defaults
	vid = 1;
	tag = 0;

	// Peek VLAN ID
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get VLAN ID
		if(str_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
		if(l > 0)																									{ vid = atoi(x); }
	}

	// Peek tag argument
	if(str_peek_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
	else if(str_cmp(x, l, "tag", 3) == 0)																			{ str_next_arg(args, args_len, 0, 0); tag = 1; }
	else																											{ /* NoOp */ }

	// Configure & apply
	scli_printf("Configuring port %i as access port in VLAN %i (%stagged)... ", port_num, vid, (tag ? "" : "un"));
	skunk_config_port_access(port_num, vid, !tag);
	skunk_config_apply();
}

// Setup Trunk Port
void skunk_scli_port_trunk(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;
	uint8_t untag;

	// Set defaults
	untag = 0;

	// Peek untag argument
	if(str_peek_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
	else if(str_cmp(x, l, "untag", 5) == 0)																			{ str_next_arg(args, args_len, 0, 0); untag = 1; }
	else																											{ /* NoOp */ }

	// Configure & apply
	scli_printf("Configuring port %i as VLAN trunk (%stagged)... ", port_num, (untag ? "un" : ""));
	skunk_config_port_trunk(port_num, untag);
	skunk_config_apply();
}

// Setup RX Sniff on Port
void skunk_scli_port_sniff_rx(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;
	uint8_t en;

	// Enable by default
	en = 1;

	// Peek enable
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get enable
		if(str_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
		if(l > 0)																									{ en = atoi(x); }
	}

	// Configure & apply
	scli_printf("%sabling RX sniff on port %i... ", (en ? "En" : "Dis"), port_num);
	skunk_config_port_sniff_rx(port_num, en);
	skunk_config_apply();
}

// Setup TX Sniff on Port
void skunk_scli_port_sniff_tx(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;
	uint8_t en;

	// Enable by default
	en = 1;

	// Peek enable
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get enable
		if(str_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
		if(l > 0)																									{ en = atoi(x); }
	}

	// Configure & apply
	scli_printf("%sabling TX sniff on port %i... ", (en ? "En" : "Dis"), port_num);
	skunk_config_port_sniff_tx(port_num, en);
	skunk_config_apply();
}

// Setup Sniff Output on Port
void skunk_scli_port_sniff_out(char **args, uint16_t *args_len, uint8_t port_num)
{
	char *x;
	uint16_t l;
	uint8_t en;

	// Enable by default
	en = 1;

	// Peek enable
	x = *args;
	l = *args_len;
	if((l > 0) && (isnum(x[0])))
	{
		// Get enable
		if(str_next_arg(args, args_len, &x, &l) == 0)																{ l = 0; }
		if(l > 0)																									{ en = atoi(x); }
	}

	// Configure & apply
	scli_printf("%sabling sniff output on port %i... ", (en ? "En" : "Dis"), port_num);
	skunk_config_port_sniff_out(port_num, en);
	skunk_config_apply();
}

// Clear Sniffing on Port
void skunk_scli_port_sniff_off(char **args, uint16_t *args_len, uint8_t port_num)
{
	// Configure & apply
	scli_printf("Disabling all sniff options on port %i... ", port_num);
	skunk_config_port_sniff_off(port_num);
	skunk_config_apply();
}
