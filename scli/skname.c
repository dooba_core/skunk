/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <scom/term.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "substrate.h"
#include "config.h"
#include "skname.h"

// Device Name
void skunk_scli_name(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Name
	if(str_next_arg(args, args_len, &x, &l) == 0)					{ l = 0; }
	if(l == 0)														{ scli_printf("%t\n", skunk_config.name, skunk_config.name_len); return; }

	// Set Name
	scli_printf("Setting device name... ");
	skunk_config_set_name(x, l);
	skunk_config_apply();
}
