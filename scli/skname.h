/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SCLI_NAME_H
#define	__SKUNK_SCLI_NAME_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Device Name
extern void skunk_scli_name(char **args, uint16_t *args_len);

#endif
