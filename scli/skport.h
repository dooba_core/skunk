/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SCLI_PORT_H
#define	__SKUNK_SCLI_PORT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Operations
#define	SKUNK_SCLI_PORT_OP_HELP					"help"
#define	SKUNK_SCLI_PORT_OP_SHOW					"show"

// Port-specific operations
#define	SKUNK_SCLI_PORT_OP_ACCESS				"access"
#define	SKUNK_SCLI_PORT_OP_TRUNK				"trunk"
#define	SKUNK_SCLI_PORT_OP_SNIFF_RX				"sniff_rx"
#define	SKUNK_SCLI_PORT_OP_SNIFF_TX				"sniff_tx"
#define	SKUNK_SCLI_PORT_OP_SNIFF_OUT			"sniff_out"
#define	SKUNK_SCLI_PORT_OP_SNIFF_OFF			"sniff_off"

// Port Management
extern void skunk_scli_port(char **args, uint16_t *args_len);

// Individual Port Management
extern void skunk_scli_port_p(char **args, uint16_t *args_len, uint8_t port_num);

// Show Help
extern void skunk_scli_port_help(char **args, uint16_t *args_len);

// Show All
extern void skunk_scli_port_show_all(char **args, uint16_t *args_len);

// Show Port
extern void skunk_scli_port_show(char **args, uint16_t *args_len, uint8_t port_num);

// Setup Access Port
extern void skunk_scli_port_access(char **args, uint16_t *args_len, uint8_t port_num);

// Setup Trunk Port
extern void skunk_scli_port_trunk(char **args, uint16_t *args_len, uint8_t port_num);

// Setup RX Sniff on Port
extern void skunk_scli_port_sniff_rx(char **args, uint16_t *args_len, uint8_t port_num);

// Setup TX Sniff on Port
extern void skunk_scli_port_sniff_tx(char **args, uint16_t *args_len, uint8_t port_num);

// Setup Sniff Output on Port
extern void skunk_scli_port_sniff_out(char **args, uint16_t *args_len, uint8_t port_num);

// Clear Sniffing on Port
extern void skunk_scli_port_sniff_off(char **args, uint16_t *args_len, uint8_t port_num);

#endif
