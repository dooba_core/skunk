/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SCLI_CONF_H
#define	__SKUNK_SCLI_CONF_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Operations
#define	SKUNK_SCLI_CONF_OP_HELP					"help"
#define	SKUNK_SCLI_CONF_OP_SHOW					"show"
#define	SKUNK_SCLI_CONF_OP_LOAD					"load"
#define	SKUNK_SCLI_CONF_OP_SAVE					"save"
#define	SKUNK_SCLI_CONF_OP_DFLT					"dflt"

// Configuration Management
extern void skunk_scli_conf(char **args, uint16_t *args_len);

// Show help
extern void skunk_scli_conf_help(char **args, uint16_t *args_len);

// Show current configuration
extern void skunk_scli_conf_show(char **args, uint16_t *args_len);

// Reload saved configuration
extern void skunk_scli_conf_load(char **args, uint16_t *args_len);

// Save current configuration
extern void skunk_scli_conf_save(char **args, uint16_t *args_len);

// Reload default configuration
extern void skunk_scli_conf_dflt(char **args, uint16_t *args_len);

#endif
