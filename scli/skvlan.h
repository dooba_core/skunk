/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SCLI_VLAN_H
#define	__SKUNK_SCLI_VLAN_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Operations
#define	SKUNK_SCLI_VLAN_OP_HELP					"help"
#define	SKUNK_SCLI_VLAN_OP_FTRK					"ftrk"
#define	SKUNK_SCLI_VLAN_OP_DTAG					"dtag"
#define	SKUNK_SCLI_VLAN_OP_VTAB					"vtab"

// VLAN Management
extern void skunk_scli_vlan(char **args, uint16_t *args_len);

// Show help
extern void skunk_scli_vlan_help(char **args, uint16_t *args_len);

// Configure double tagging
extern void skunk_scli_vlan_dtag(char **args, uint16_t *args_len);

// Configure full trunking
extern void skunk_scli_vlan_ftrk(char **args, uint16_t *args_len);

// VLAN Table Management
extern void skunk_scli_vlan_vtab(char **args, uint16_t *args_len);

#endif
