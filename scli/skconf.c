/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <scom/term.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "substrate.h"
#include "config.h"
#include "skconf.h"

// Configuration Management
void skunk_scli_conf(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;

	// Acquire Operation
	if(str_next_arg(args, args_len, &x, &l) == 0)																	{ l = 0; }
	if(l == 0)																										{ scli_printf("Invalid operation - use 'skconf help'\n"); }
	else if(str_cmp(x, l, SKUNK_SCLI_CONF_OP_HELP, strlen(SKUNK_SCLI_CONF_OP_HELP)) == 0)							{ skunk_scli_conf_help(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_CONF_OP_SHOW, strlen(SKUNK_SCLI_CONF_OP_SHOW)) == 0)							{ skunk_scli_conf_show(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_CONF_OP_LOAD, strlen(SKUNK_SCLI_CONF_OP_LOAD)) == 0)							{ skunk_scli_conf_load(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_CONF_OP_SAVE, strlen(SKUNK_SCLI_CONF_OP_SAVE)) == 0)							{ skunk_scli_conf_save(args, args_len); }
	else if(str_cmp(x, l, SKUNK_SCLI_CONF_OP_DFLT, strlen(SKUNK_SCLI_CONF_OP_DFLT)) == 0)							{ skunk_scli_conf_dflt(args, args_len); }
	else																											{ scli_printf("Invalid operation '%t' - use 'skconf help'\n", x, l); }
}

// Show help
void skunk_scli_conf_help(char **args, uint16_t *args_len)
{
	// Show help
	scli_printf("Usage: skconf <OPERATION>\n");
	scli_printf("\nOperations:\n");
	scli_printf(" * help - Display this help\n");
	scli_printf(" * show - Display current configuration\n");
	scli_printf(" * load - Reload saved configuration\n");
	scli_printf(" * save - Save current configuration\n");
	scli_printf(" * dflt - Reload default configuration\n");
}

// Show current configuration
void skunk_scli_conf_show(char **args, uint16_t *args_len)
{
	uint8_t i;
	struct skunk_config_port *p;

	// Show config
	scli_printf("Device name: %t\n", skunk_config.name, skunk_config.name_len);
	scli_printf("VLAN trunking: %s\n", (skunk_config.vlan_ft ? "full" : "on-demand"));
	scli_printf("VLAN double tagging: %s\n", (skunk_config.vlan_dt ? "on" : "off"));
	for(i = 0; i < SKUNK_PORTS; i = i + 1)
	{
		p = &(skunk_config.ports[i]);
		scli_printf("Port %i: %s", i, ((p->flags & SKUNK_CONFIG_PORT_TRUNK) ? "trunk" : "access"));
		if(p->flags & SKUNK_CONFIG_PORT_TRUNK)
		{
			if(p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG)																{ scli_printf(" untag"); }
		}
		else
		{
			if((p->flags & SKUNK_CONFIG_PORT_VLAN_UNTAG) == 0)														{ scli_printf(" tag"); }
			scli_printf(" (VLAN %i)", p->vlan_id);
		}
		if(p->flags & SKUNK_CONFIG_PORT_SNIFF_MASK)
		{
			scli_printf(" - sniff {");
			if(p->flags & SKUNK_CONFIG_PORT_SNIFF_RX)																{ scli_printf(" rx"); }
			if(p->flags & SKUNK_CONFIG_PORT_SNIFF_TX)																{ scli_printf(" tx"); }
			if(p->flags & SKUNK_CONFIG_PORT_SNIFF_OUT)																{ scli_printf(" out"); }
			scli_printf(" }");
		}
		scli_printf("\n");
	}
}

// Reload saved configuration
void skunk_scli_conf_load(char **args, uint16_t *args_len)
{
	// Load
	scli_printf("Reloading saved configuration... ");
	skunk_config_load();
	skunk_config_apply();
}

// Save current configuration
void skunk_scli_conf_save(char **args, uint16_t *args_len)
{
	// Save
	scli_printf("Saving current configuration...\n");
	skunk_config_save();
}

// Reload default configuration
void skunk_scli_conf_dflt(char **args, uint16_t *args_len)
{
	// Load default
	scli_printf("Reloading default configuration... ");
	skunk_config_load_default();
	skunk_config_apply();
}
