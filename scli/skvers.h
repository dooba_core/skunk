/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_SCLI_VERS_H
#define	__SKUNK_SCLI_VERS_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Firmware Version
extern void skunk_scli_vers(char **args, uint16_t *args_len);

#endif
