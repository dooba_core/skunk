/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

#ifndef	__SKUNK_H
#define	__SKUNK_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Ethernet Ports
#define	SKUNK_PORTS						4

// Init LED Delay (ms)
#define	SKUNK_INIT_LED_DELAY			25

// LEDs
#define	SKUNK_LED_STATUS				18
#define	SKUNK_LED_ACT					19

// Main Initialization (eloop)
extern void init();

// Main Loop (eloop)
extern void loop();

// Init LEDs
extern void skunk_init_leds();

#endif
