/* Skunk
 * Firmware for the Skunk Gigabit Ethernet switch
 */

// External Includes
#include <ksz9896/regs.h>
#include <ksz9896/monitor.h>

// Internal Includes
#include "substrate.h"
#include "sniff.h"

// Set RX sniff on port
void skunk_sniff_port_rx(uint8_t port_num, uint8_t enable)
{
	// Configure RX sniff
	ksz9896_monitor_set_sniff_rx(sw0, port_num, enable);
}

// Set TX sniff on port
void skunk_sniff_port_tx(uint8_t port_num, uint8_t enable)
{
	// Configure TX sniff
	ksz9896_monitor_set_sniff_tx(sw0, port_num, enable);
}

// Set sniff output on port
void skunk_sniff_port_out(uint8_t port_num, uint8_t enable)
{
	// Configure sniff output
	ksz9896_monitor_set_out_port_s(sw0, port_num, enable);
}
